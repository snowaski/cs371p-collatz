// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream> // istringtstream, ostringstream
#include <tuple>   // make_tuple, tuple
#include <utility> // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read)
{
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// cycle_length
// ----

TEST(CollatzFixture, cl0)
{
    ASSERT_EQ(collatz_cycle_length(1), 1);
}

TEST(CollatzFixture, cl1)
{
    ASSERT_EQ(collatz_cycle_length(7), 17);
}

TEST(CollatzFixture, cl2)
{
    ASSERT_EQ(collatz_cycle_length(100), 26);
}

TEST(CollatzFixture, cl3)
{
    ASSERT_EQ(collatz_cycle_length(999999), 259);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0)
{
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1)
{
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2)
{
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3)
{
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4)
{
    ASSERT_EQ(collatz_eval(make_pair(684454, 227935)), make_tuple(684454, 227935, 509));
}

TEST(CollatzFixture, eval5)
{
    ASSERT_EQ(collatz_eval(make_pair(10, 157)), make_tuple(10, 157, 122));
}

// ----
// check_cache
// ----

TEST(CollatzFixture, check0)
{
    ASSERT_EQ(collatz_check_cache(600000), 0);
}

TEST(CollatzFixture, check1)
{
    ASSERT_EQ(collatz_check_cache(7), 17);
}

TEST(CollatzFixture, check2)
{
    ASSERT_EQ(collatz_check_cache(1123), 0);
}

// ----
// add_to_cache
// ----

TEST(CollatzFixture, add0)
{
    ASSERT_EQ(collatz_add_to_cache(600000, 1), false);
}

TEST(CollatzFixture, add1)
{
    ASSERT_EQ(collatz_add_to_cache(100, 26), true);
}

TEST(CollatzFixture, add2)
{
    collatz_add_to_cache(100, 26);
    ASSERT_EQ(collatz_check_cache(100), 26);
}

TEST(CollatzFixture, add3)
{
    collatz_add_to_cache(600000, 1);
    ASSERT_EQ(collatz_check_cache(600000), 0);
}

// -----
// print
// -----

TEST(CollatzFixture, print)
{
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve)
{
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
