// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read(const string &);

// ------------
// collatz_add_to_cache
// ------------

/**
 * add a number to cache if in range
 * @param a long
 * @param an int
 * @return a boolean
 */
bool collatz_add_to_cache(long n, int c);

// ------------
// collatz_check_cache
// ------------

/**
 * check if a number is in the cache
 * @param a long
 * @return an int
 */
int collatz_check_cache(long n);

// ------------
// collatz_cycle_length
// ------------

/**
 * compute the collatz cycle length for a given n
 * @param an int
 * @return an int
 */
int collatz_cycle_length(long n);

// ------------
// collatz_eval
// ------------

/**
 * finds the max collatz cycle length for a range of numbers
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval(const pair<int, int> &);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print(ostream &, const tuple<int, int, int> &);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve(istream &, ostream &);

#endif // Collatz_h
