// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

const int MAX_CACHE_SIZE = 500000;
int cache[MAX_CACHE_SIZE];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read(const string &s)
{
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_add_to_cache
// ------------

bool collatz_add_to_cache(long n, int c)
{
    if (n < MAX_CACHE_SIZE)
    {
        cache[n] = c;
        return true;
    }
    else
        return false;
}

// ------------
// collatz_check_cache
// ------------

int collatz_check_cache(long n)
{
    if (n < MAX_CACHE_SIZE)
        return cache[n];
    else
        return 0;
}

// ------------
// collatz_cycle_length
// ------------

int collatz_cycle_length(long n)
{
    assert(n > 0);
    int c = 1;

    // if a number is odd, automatically multiply by 3 and add 1 and divide by two
    if (n != 1 && (n % 2) != 0)
    {
        n = n + n / 2 + 1;
        c += 2;
    }

    while (n > 1)
    {
        int cache_value = collatz_check_cache(n);
        // if a value is found that's already in the cache, calculate cycle length from there
        if (cache_value != 0)
        {
            return c + cache_value - 1;
        }
        if ((n % 2) == 0)
            n /= 2;
        else
        {
            n = (3 * n) + 1;
            // assure no overflow
            assert(n > 0);
        }
        ++c;
    }

    assert(c > 0);
    return c;
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval(const pair<int, int> &p)
{
    int i, j;
    tie(i, j) = p;
    int initial_i = i;
    int initial_j = j;

    assert(i > 0 && j > 0);
    assert(i < 1000000 && j < 1000000);

    // ensures range is in increasing order
    if (i > j)
    {
        i = j;
        j = initial_i;
    }

    // optimization to cut range in half if condition met
    int m = (j / 2) + 1;
    if (i < m)
        i = m;

    int max = 0;
    for (int n = i; n <= j; n++)
    {
        int c = collatz_check_cache(n);
        if (c == 0)
        {
            c = collatz_cycle_length(n);
            collatz_add_to_cache(n, c);
        }
        if (c > max)
            max = c;
    }

    assert(max > 0);

    return make_tuple(initial_i, initial_j, max);
}

// -------------
// collatz_print
// -------------

void collatz_print(ostream &sout, const tuple<int, int, int> &t)
{
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve(istream &sin, ostream &sout)
{
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
